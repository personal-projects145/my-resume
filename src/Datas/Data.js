export const Data = {
  profile: {
    name: "Rem Ermita",
    ocupation: "Junior Full Stack Software Developer",
    location: "Manila, Philippines",
    email: "remi.ermita@gmail.com",
    telephone: "+63 9568775259",
    image: "images/rem.jpeg",
  },
  aboutMe: {
    label: "Profile",
    description:
      "A Web Developer that is training to develop variety of applications in multiple programming languages.",
  },
  skills: {
    technicalLabel: "Tech Stack",
    softLabel: "Other Skills",
    technicalSkills: [
      "JavaScript",
      "Git",
      "PHP",
      "SQL",
      "ReactJS",
      "NodeJS",
      "HTML/CSS"
    ],
    softSkills: [
      "Adobe Photoshop",
      "Adobe Illustrator",
      "Adobe InDesign",
      "Figma"
    ],
  },
  socialMedia: {
    label: "SOCIAL",
    social: [
      {
        label: "LinkedIn",
        name: "linkedin",
        url: "https://www.linkedin.com/in/rem-ermita-0048/",
        className: "bxl-linkedin-square",
      },
      {
        label: "GitLab",
        name: "gitlab",
        url: "https://gitlab.com/rmermita",
        className: "bxl-gitlab",
      },
      {
        label: "Discord",
        name: "Discord",
        url: "https://discord.com/users/386248821941993473",
        className: "bxl-discord",
      },
    ],
  },
  experience: {
    works: [
      {
        title: "FRONT END DEVELOPER",
        period: "March 2021 - July 2021",
        company: "Educare Guardian",
        description: [
          "Development of Frontend with using  HTML/CSS, Bootstrap",
          "Included a bilingual website with a button that can easily be changed."
        ],
      },
      {
        title: "FRONT END DEVELOPER",
        period: "November 2021 - January 2022",
        company: "Not allowed to disclose",
        description: [
          "Development of Frontend with using  JavaScript, HTML/CSS, Bootstrap"
        ],
      },
    ],
    academic: [
      {
        career: "Full Stack Web Developer",
        date: "2021",
        institution: "Zuitt Coding Bootcamp",
      },
      {
        career: "B.S. Nursing",
        date: "2013",
        institution: "Our Lady of Fatima University",
      },
    ],
    proyects: [
      {
        name: "Online Portfolio",
        company: "Capstone 1",
        period: "Nov. 2021",
        description: [
          "Created a minimalist online portfolio with 3 different external nav bars using HTML/CSS as part of our capstone."
        ],
      },
      {
        name: "Backend E-commerce Development",
        company: "Capstone 2",
        period: "Jan. 2022",
        description: [
          "Created a backend data for e-commerce using NodeJS, MongoDB, JavaScript. And deployed using heroku."
        ],
      },
      {
        name: "E-commerce React App",
        company: "Capstone 3",
        period: "March 2022",
        description: [
          "Created an e-commerce frontend using ReactJS and the backend data that we created on our previous capstone."
        ],
      },
      {
        name: "Web Development",
        company: "Not allowed to disclose",
        period: "July 2021",
        description: [
          "Rebuilt a 3-year old website using JavaScript, HTML/CSS",
          "Added a Chinese language on the website using JavaScript."
        ],
      }
    ],
  },
};
